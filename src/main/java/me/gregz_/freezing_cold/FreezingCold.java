package me.gregz_.freezing_cold;

import org.bukkit.plugin.java.JavaPlugin;

import me.gregz_.freezing_cold.threading.DamagePlayer;

public class FreezingCold extends JavaPlugin{
	
	private DamagePlayer damagePlayer;
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		damagePlayer = new DamagePlayer(this);
	}
	
	// API
	public DamagePlayer getDamagePlayer() {
		return damagePlayer;
	}
}
