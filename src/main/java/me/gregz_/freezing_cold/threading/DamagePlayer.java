package me.gregz_.freezing_cold.threading;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import me.gregz_.freezing_cold.FreezingCold;

public class DamagePlayer extends BukkitRunnable {

	private final int damageInterval;
	private final List<String> damageBiomes = new ArrayList<String>();
	private final int warmthCheckRange;
	private final double damageAmount;
	private final String coldMessage;
	
	public DamagePlayer(FreezingCold plugin) {
		damageInterval = plugin.getConfig().getInt("Damage_Interval", 5);
		damageBiomes.addAll(plugin.getConfig().getStringList("Damage_Biomes"));
		warmthCheckRange = plugin.getConfig().getInt("Warmth_Check_Range", 5);
		damageAmount = plugin.getConfig().getDouble("Damage_Amount", 1);
		coldMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Cold_Message", "&9You're Cold"));
		this.runTaskTimer(plugin, damageInterval * 20, damageInterval * 20);
	}

	@Override
	public void run() {
		for (final Player p : Bukkit.getOnlinePlayers()) {
			if (p.getItemInHand().getType().equals(Material.TORCH)) {
				continue;
			} else if (leatherArmour(p.getInventory().getArmorContents())) {
				continue;
			} else if (!inDammageBiome(p.getLocation().getBlock().getBiome().toString())) {
				continue;
			} else if (inRangeOfWarmth(p.getLocation(), warmthCheckRange)) {
				continue;
			} else {
				p.sendMessage(coldMessage);
				p.damage(damageAmount);
			}
		}
	}

	private boolean inDammageBiome(String biome) {
		for (String s : damageBiomes) {
			if (s.equalsIgnoreCase(biome)) {
				return true;
			}
		}
		return false;
	}

	private boolean leatherArmour(ItemStack[] armour) {
		for (ItemStack is : armour) {
			if (is.getType().equals(Material.LEATHER_HELMET) || is.getType().equals(Material.LEATHER_CHESTPLATE) || is.getType().equals(Material.LEATHER_LEGGINGS) || is.getType().equals(Material.LEATHER_BOOTS)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean inRangeOfWarmth(Location centerBlock, int radius) {
		int bx = centerBlock.getBlockX();
		int by = centerBlock.getBlockY();
		int bz = centerBlock.getBlockZ();
		int radiusSquared = radius * radius;

		for(int x = bx - radius; x <= bx + radius; x++) {
			for(int y = by - radius; y <= by + radius; y++) {
				for(int z = bz - radius; z <= bz + radius; z++) {                  
					if(((bx-x) * (bx-x) + ((bz-z) * (bz-z)) + ((by-y) * (by-y))) < radiusSquared) {
						Material mat = new Location(centerBlock.getWorld(), x, y, z).getBlock().getType();
						if (mat.equals(Material.TORCH) || mat.equals(Material.FIRE)){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
